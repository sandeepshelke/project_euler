Spoiler alert: These are answers to the project_euler problems. Open at risk of 
losing an opportunity to learn.
 
Solving problems at http://projecteuler.net/
Using python2 and python3.

Disclaimer: I started using Python extensively while solving the examples at
checkio. I'm not good at Python and still learning.

Invitation:
Feel free to use these scripts as you wish. A little mention of my
github repo should be enough, but it isn't necessary. :)
Please review the code and suggest changes.

Disclaimer:
If you think I copied some of your code then please mention it.
I'll be more than happy to add your name.