'''
Take the number 192 and multiply it by each of 1, 2, and 3:
192  1 = 192
192  2 = 384
192  3 = 576
By concatenating each product we get the 1 to 9 pandigital, 192384576. 
We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, 
giving the pandigital, 918273645, which is the concatenated product of 9 and 
(1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the 
concatenated product of an integer with (1,2, ... , n) where n  1?
'''

def is_pandigital(num):
    s = str(num)
    return not '0' in s and len(s) == 9 and len(set(list(s))) == 9

pan, num, pan_curr, pan_num = 0, 0, 0, 0
while pan_curr < 987654321 and num < 9876:
    num, mult = num+1, 1
    pan, products = num, []
    while len(str(pan)) < 9:
        mult += 1
        products += [num*mult]
        pan = int(str(pan)+str(products[-1]))

    if is_pandigital(pan):
        print(num, mult, products, pan)
        if pan_curr < pan:
            pan_curr = pan
            pan_num = num

print (pan_curr, pan_num)
