from pyprimes import isprime

def generate_primes(r):
    prime = [i for i in range(r) if isprime(i)]
    return prime

def calc(a, b):
    c, n = 0, 0
    while isprime(n**2+n*a+b):
        c += 1
        n += 1
    return c

p = generate_primes(1000)

ps = []
a_max, b_max, c_max = 0, 0, 0 
for a in range(-1000,1000):
    for b in p:
        if b < -1600-40*a or b < c_max: continue
        c = calc(a, b)
        if c > c_max:
            a_max, b_max, c_max = a, b, c

print a_max, b_max, c_max, a_max*b_max