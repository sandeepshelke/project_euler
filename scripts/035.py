"""
The number, 197, is called a circular prime because all rotations of the
digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37,
71, 73, 79, and 97.

How many circular primes are there below one million?
"""
from utility import Math

lbound = 101
ubound = 1000000

def get_circular(n):
    nc = [n]
    for _ in range(len(str(n))-1):
        n = int(str(n%10)+str(n//10))
        nc += [int(n)]
    return nc

cps = [2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, 97]
for n in range(lbound, ubound):
    if n in cps:
        continue
    if Math.is_prime(n):
        circ = get_circular(n)
        is_circ = True
        for n in circ[1:]:
            if not Math.is_prime(n):
                is_circ = False
                break
        if is_circ:
            cps += circ

print(cps, len(cps))