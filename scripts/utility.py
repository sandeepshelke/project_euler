class Math:
    """ Provides basic mathematical utilities
    """
    @staticmethod
    def is_palindrome(n):
        s = str(n)
        if s == s[::-1]:
            return True
        return False
    
    @staticmethod
    def get_binary(n):
        return int(bin(n)[2:])

    @staticmethod
    def is_prime(n):
        """Determines whether number is prime number
        Args:
            n imput integer value

        Returns:
            True if number is prime, False otherwise
        """
        if n == 2:
            return True
        if not n & 1:
            return False
        if n < 2:
            return False

        for x in range(3, int(n**0.5)+1, 2):
            if n % x == 0:
                return False
        return True

    @staticmethod
    def factorize(n):
        """Calculates factors of the input number
        Returns list of factors
        """
        f = []
        for i in range(1, n/2+1):
            if n%i == 0:
                f += [i]
        return f

    @staticmethod
    def sumFactors(n):
        """Calculates sum of factors of the input number
        Returns sum
        """
        return sum(Math.factorize(n))

    @staticmethod
    def factors(n):
        """
        Calculates factors of input number
        """
        return Math.factorize(n)

    @staticmethod
    def factorial(n):
        """Calculates factorial of input number
        """
        p = 1
        for i in range (1, n+1):
            p *= i
        return p
    
    @staticmethod
    def digits(n):
        """Splits the number in digits
        returns the list of digits in order
        e.g. input: 123 then output: [1,2,3,4]
        """
        d = []
        while n > 0:
            d += [n%10]
            n = n//10
        return d
    
    @staticmethod
    def combs(f):
        c = [] + f
        for i in range(len(f)):
            for j in range(i+1, len(f)):
                c += [str(f[i])+str(f[j])]
        return sorted(c)
