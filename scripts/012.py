import pyprimes
from collections import Counter

i, n = 1, 0
while 1:
    n = n + i
    f = pyprimes.factors(n)
    c = reduce(lambda x, y: x * y, [x+1 for x in Counter(f).values()])
    if c >= 500:
        print n
        break
    i += 1
