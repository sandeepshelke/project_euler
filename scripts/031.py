# target = 200
# coinSizes = [1, 2, 5, 10, 20, 50, 100, 200]
# ways = [1]*201
# ways[0] = 1
# 
# for i in range(8):
#     for j in range(coinSizes[i], target+1):
#         ways[j] += ways[j - coinSizes[i]]
#     print(ways)
#         
def nway( total, coins):
    c, coins = coins[0], coins[1:]
    count = 0 
    if total % c == 0: count += 1    
    for amount in range( 0, total, c):
        count += nway(total - amount, coins)    
    return count
# main
print (nway( 200, (1,2,5,10,20,50,100,200)))