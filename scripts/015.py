from math import log
from math import factorial
 
def log_fac(n):
    p = 0
    for i in range(2,n+1):
        p *= i
    return p
 
def log_binomial(n,k): 
    return (log_fac(n)-log_fac(k)-log_fac(n-k))

n = 40
k = 20
r = factorial(n)/(factorial(k)*factorial(n-k))
print r
