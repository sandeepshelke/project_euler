'''
We shall say that an n-digit number is pandigital if it makes use of all the
digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is
also prime.

What is the largest n-digit pandigital prime that exists?
'''
from utils import Math
from itertools import permutations

def pandigital():
    digits = '9876543210'
    prime = 0
    for num in permutations(digits):
        prime = int(''.join(num))
        if Math.is_prime(prime):
            break
    return prime

print pandigital()
