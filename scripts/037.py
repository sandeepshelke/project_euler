'''
The number 3797 has an interesting property. Being prime itself, it is
possible to continuously remove digits from left to right, and remain
prime at each stage: 3797, 797, 97, and 7. Similarly we can work from
right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from
left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
'''
from utils import Math

def is_truncatable(prime):
    l2r = str(prime)
    r2l = str(prime)
    while len(l2r) and len(r2l):
        if not Math.is_prime(int(l2r)) or not Math.is_prime(int(r2l)):
            return False
        l2r = l2r[:-1]
        r2l = r2l[1:]
    return True

num = 10
count = 0
tprime = []

while count < 11:
    if Math.is_prime(num) and is_truncatable(num):
        tprime += [num]
        count += 1
    num += 1

print(sum(tprime))
print(tprime)
