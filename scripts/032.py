#Sum of products which can be represented as pan-digital
#like The product 7254 is unusual, as the identity, 39 × 186 = 7254, 
#containing multiplicand, multiplier, and product is 1 through 9 pan-digital.

def isPanDigital(n):
    #The n digit number should have digits from 1 to n, no duplicates
    s = str(n)
    sset = set(s)
    length = len(s)
    if length != len(sset):
        return False
    for d in sset:
        if int(d) < 1 or int(d) > length:
            return False
    return True

prods = {}
prodsum = 0
for mpd in range(2, 100):
    for mpr in range(123 if mpd>9 else 1234, 10000//mpd+1):
        p = mpd*mpr
        s = str(mpd)+str(mpr)+str(p)
        if isPanDigital(int(s)):
            if not prods.get(p, None):
                prods[p] = str(mpd)+'X'+str(mpr)
                prodsum += p

print(prodsum)
print(prods)
