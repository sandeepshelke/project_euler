month_name = {4:'April',6:'June',9:'September',11:'November',1:'January',3:'March',5:'May',7:'July',8:'August',10:'October',12:'December',2:'February'}
months = [0,31,28,31,30,31,30,31,31,30,31,30,31]
days = {1:'Monday',2:'Tuesday',3:'Wednesday',4:'Thursday',5:'Friday',6:'Saturday',7:'Sunday'}
week_days ={'Monday':1,'Tuesday':2,'Wednesday':3,'Thursday':4,'Friday':5,'Saturday':6,'Sunday':7}

def isLeap(year):
	if ((year%4 == 0 and year%100!=0) or (year%400==0)):
		return True
	return False

def getLeapYears(start, end):
	leaps = []
	for year in range(start, end+1):
		if isLeap(year):
			leaps += [year]
	return leaps

def getDays(m, year):
	days = months[m]
	if isLeap(year) and month_name[m] == 'February':
		days += 1
	return days

def calculateStartEndDays(year, month, days):
	
	return start, end

#as per input
start_date = 1
start_day = days[start_date]
start_month = month_name[1]
start_year = 1901

end_date = 31
end_month = month_name[12]
end_year = 2000

leaps = getLeapYears(start_year, end_year)
total_days = 100*365+len(leaps)
#for d in range(1, total_days+1):

new_month = []
mc = 0
for year in range(start_year, end_year+1):
	for m in range(1,13):
		if year == 1901 and m == 1:
			new_month = [[1, 31, 'Tuesday', 'January']]
		else:
			sd = new_month[mc][0]+new_month[mc][1]
			numd = getDays(m, year)
			dn = (week_days[new_month[mc][2]]+new_month[mc][1]%7)
			if dn > 7:
				dn = dn%7
			day = days[dn]
			new_month += [[sd, numd, day, month_name[m]]]
			mc += 1

count=0
for mon in new_month:
	if 'Sunday' in mon:
		count += 1
		print mon
print count