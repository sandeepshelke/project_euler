from pyprimes import isprime
from math import sqrt

def divisors(n):
    factors = [1, n]
    if isprime(n): return factors

    s = 2
    rt = sqrt(n)
    while s < rt:
        if n%s == 0:
            factors += [s, n/s]
        s+=1
    if rt == s:
        factors += [s]
    return factors

smf = {1:1, 2:3, 3:4, 4:7, 5:6, 6:12, 7:8, 8:15, 9:13, 10:18}

def sumer(r):
    s = 0
    i = 1
    while i <= r:
        j = 1
        while j <= r:
            p = i*j
            c = 0
            try:
                c = smf[p]
            except KeyError:
                c = sum(divisors(p))
                smf[p] = c
            s += c
            j += 1
        i += 1

    return s

base = 10**11
num = base*base
print num, sumer(num)
#print smf
