# Sum of non-abundant
from utility import Math

ulimit = 28124

abundant = []
alls = [True]*ulimit
for n in range(1, ulimit):
    s = Math.sumFactors(n)
    if s > n+n:
        abundant += [n]

for i in range(len(abundant)):
    for k in range(n+1, len(abundant)):
        sum2 = abundant[i] + abundant[k]
        if sum2 < ulimit and sum2 > 0:
            alls[sum2] = False

sumNA = 0
for i in range(1, ulimit):
    if alls[i]:
        sumNA += i

print 'number of abundants = ' + str(len(abundant))
print 'sum of non-abundants = ' + str(sumNA)
