def cycle_length(n):
    i = 1
    if n % 2 == 0: 
        print n
        return cycle_length(n / 2)
    if n % 5 == 0: 
        return cycle_length(n / 5)

    while True:
        print i, n
        if (pow(10, i) - 1) % n == 0: 
            return i
        else: 
            i += 1

c = cycle_length(0.3333333333333333333)
print c
exit()

m = 0
n = 0
for d in xrange(1,1000):
    c = cycle_length(d)
    if c > m:
        m = c
        n = d

print n