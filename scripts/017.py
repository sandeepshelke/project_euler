units = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
teens = ["", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
tens = ["", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

def num2word(num):
    if num == 1000:
        return 'one thousand'
    h = num/100
    tu = num%100
    word = ''
    if h > 0:
        word = units[h] + ' hundred'
        if tu > 0:
            word += ' and '
        else:
            return word

    if tu <= 19:
        if tu == 10:
            word += tens[tu/10]
        elif tu < 10:
            word += units[tu]
        else:
            word += teens[tu%10]
        return word

    t = tu/10
    u = tu%10
    word += tens[t]
    if u > 0:
        word += '-' + units[u]
    return word

words = []
stripped = []
count = 0
for i in range(1,1001):
    w = num2word(i)
    words += [w]
    stripped += (w.replace(' ','')).replace('-','')
    count += len((w.replace(' ','')).replace('-',''))
    print count

print count
