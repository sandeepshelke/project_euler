"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""

def is_prime(n):
    if n == 2:
        return True
    if not n & 1:
        return False
    if n < 2:
        return False

    for x in range(3, int(n**0.5)+1, 2):
        if n % x == 0:
            return False
    return True

def factors(n):
    """Calculates factors of the input number
    Returns list of factors
    """
    return [i for i in range(1, n//2+1) if n%i == 0]

def project_euler_org(limit):
    d = factors(limit)
    d = d[::-1]
    for i in d:
        if is_prime(i):
            return i

def project_euler(limit):
    """I found this solution at s-anand.net
    This is so crisp and fast that I thought I should mention it here.
    My above crude way takes lots of time to solve this problem.
    But this one is really fast
    """
    i = 2
    while i*i < limit:
        while limit%i == 0:
            limit = limit//i
        i = i + 1
    return limit

if __name__ == '__main__':
    limit = 600851475143
    print(project_euler(limit))

