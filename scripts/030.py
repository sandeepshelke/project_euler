def calc(num, exp):
    s=0
    for c in str(num):
        s += int(c)**exp
    return s

n4 = []

for num in range(9,999999):
    s = calc(num, 5)
    if s == num:
        n4 += [num]

print n4, sum(n4)