# Euclid's formula
# a = m^2 - n^2
# b = 2mn
# c = m^2 + n^2

a = 0
b = 0
c = 0
for m in range(25):
    for n in range(m+1):
        a = m**2 - n**2
        b = 2*m*n
        c = m**2 + n**2
        if a+b+c == 1000:
            print a*b*c, a, b ,c
            break
    if a+b+c == 1000:
        break
