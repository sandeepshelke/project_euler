def factors(n):
	f = []
	sum = 0
	for i in range(1,n/2+1):
		if n%i == 0:
			f += [i]
			sum += i
	return f, sum

num = []
sum = []
for n in range(1,10000):
	f, sum = factors(n)
	num += [[n, sum]]

amicable = []
sum = 0
for i in range(len(num)):
	for j in range(i+1, len(num)):
		if i == j:
			continue
		if num[i][0] == num[j][1] and num[i][1] == num[j][0] and num[i][0] != num[j][0]:
			amicable += [num[i][0], num[j][0]]
			sum += num[i][0] + num[j][0]

print amicable
print sum