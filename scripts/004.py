"""
A palindromic number reads the same both ways. The largest palindrome made
from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""

def is_palindrome(s):
	return s == s[::-1]

def project_euler():
	n = 999
	num = 0
	i = n
	while i >= 900:
		pal = False
		j = n
		while j >= 900:
			num = i * j
			if is_palindrome(str(num)):
				pal = True
				break
			j -= 1
		if pal:
			break
		i -= 1
	return num

if __name__ == '__main__':
	print(project_euler())
