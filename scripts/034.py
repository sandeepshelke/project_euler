"""
145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
"""

lbound = 10
ubound = 2540161

def factorial(n):
    p = 1
    for i in range (1, n+1):
        p *= i
    return p

def digits(n):
    """Splits the number in digits
    This is a generator function. That means returns one element at a time
    returns the list of digits in order
    e.g. input: 123 then output: [1,2,3,4]
    """
    while n > 0:
        yield n%10
        n = n//10

def sumOfFactOfDigits(n):
    global facts
    s = 0
    for i in digits(n):
        s += facts[i]
    return s

facts = []
for i in range(10):
    facts += [factorial(i)]

s = 0
for i in range(lbound, ubound):
    if i == sumOfFactOfDigits(i):
        s += i 
    
print(s)
