
def getCollatzSq(i):
    chain = []
    while True:
        if i == 1:
            break
        if i%2 == 0:
            i=i/2
        else:
            i=i*3+1
        chain += [i]
    return [len(chain),chain]

length = 1
prev_length = [1, length]
for i in range(1,1000001):
    length = getCollatzSq(i)[0]
    if length > prev_length[1]:
        print i, length
        prev_length = [i, length]
print prev_length
