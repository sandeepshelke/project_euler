'''
If p is the perimeter of a right angle triangle with integral length
sides, {a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p  1000, is the number of solutions maximised?
'''
from math import sqrt

def get_sides(p):
    sides = []
    for a in range(1, p/2):
        b = (p*p - 2*p*a)/(2*p-2*a)
        c = sqrt(a*a + b*b)
        if a+b+c == p:
            sides += [(a,b,int(c))]
    return sides

count, perimeter, legs = 0, 0, []
for peri in range(1, 1001):
    s = get_sides(peri)
    if len(s) > count:
        count = len(s)
        legs = s
        perimeter = peri

print(perimeter, count, legs)
        
