import math

def is_prime(n):
	n = abs(long(n))
	if n < 2:
		return False
	if n == 2: 
		return True	
	if not n & 1: 
		return False
	for x in range(3, long(n**0.5)+1, 2):
		if n % x == 0:
			return False
	return True

def highest_expo(n, limit):
	num = n
	count = 1
	while True:
		num *= n
		if num > limit:
			break
		count += 1
	return count

primes = []
limit = 20
lcm = 1
for i in range (limit):
	if is_prime(i):
		primes += [i]

print primes

for i in primes:
	he = highest_expo(i, limit)
	print i, he
	lcm = lcm * math.pow(i, he)

print int(lcm)

